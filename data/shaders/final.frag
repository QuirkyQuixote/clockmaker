#version 420 core

in vec2 frag_texcoords;

out vec4 color;

layout(binding = 0) uniform sampler2D color_tex;
layout(binding = 1) uniform sampler2D distortion_tex;
uniform float pixel_size;
uniform int scanlines;

void main()
{
        vec2 dist = texture(distortion_tex, frag_texcoords).rg - 0.5;
        vec2 real_texcoords = frag_texcoords + dist * 0.1;
        vec2 real_fragcoord = gl_FragCoord.xy + dist * pixel_size * 10;
        vec3 col = texture(color_tex, real_texcoords).rgb;
        color = vec4(col, 1.0);

        if (scanlines == 1) {
                float off_y = fract(real_fragcoord.y / pixel_size);
                float y = 1 - abs(1 - off_y * 2);
                color *= (0.67 + 0.33 * y);
                color *= 1.5;
        } else if (scanlines == 2) {
                float off_x = fract(real_fragcoord.x / pixel_size);
                float off_y = fract(real_fragcoord.y / pixel_size);
                float x = 1 - abs(1 - off_x * 2);
                float y = 1 - abs(1 - off_y * 2);
                color *= (0.67 + 0.33 * y);
                color *= (0.67 + 0.33 * x);
                color *= 1.5;
        } else if (scanlines == 3) {
                float off_x = fract(real_fragcoord.x / pixel_size);
                float off_y = fract(real_fragcoord.y / pixel_size);
                float x = 1 - abs(1 - off_x * 2);
                float y = 1 - abs(1 - off_y * 2);
                color *= (0.67 + 0.33 * y);
                color *= 1.5;
                color.r *= 0.33 + (1 - off_x);
                color.g *= 0.33 + 1 - 2 * abs(off_x - 0.5);
                color.b *= 0.33 + off_x;
        }
}
