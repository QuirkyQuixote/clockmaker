-- untitled.lua - behavior file for untitled.json

-- Copyright (C) 2022 L. Sanz <luis.sanz@gmail.com>

-- This file is part of Clockmaker.

-- Clockmaker is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- Clockmaker is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.

-- You should have received a copy of the GNU Affero General Public License
-- along with Clockmaker.  If not, see <https://www.gnu.org/licenses/>.

-- Any code executed at top level will run when the scene is loaded

context:say("\x01: jump\n" ..
            "\x03: use/attack\n" ..
            "\x0C\x0D\x0E\x0F: move")

-- This table stores persistent data for mobs

persistent = {}

-- High-level management of the audio queues

audio_queues = {
        segments = {
                zwosta = {0,2000,24},
                tunnel = {102000,2000,32},
                office = {50000,2000,16},
                zwosta_tunnel = {100000,2000,1},
                tunnel_tunnel = {102000,2000,32},
                _fall = {170000,2000,9},
                zwosta_office = {48000,2000,1},
                tunnel_zwosta = {166000,2000,1},
                tunnel_fall = {168000,2000,1},
                office_zwosta = {82000, 2000, 1},
        },
        cur_seq = "",
}

function audio_queues:transition(new_seq)
        if new_seq == self.cur_seq then
                return
        end
        s = self.segments[self.cur_seq .. "_" .. new_seq];
        if s then
                context:music(s[1], s[2], s[3])
        end
        self.cur_seq = new_seq
end

function audio_queues:update()
        if context:needs_music() then
                s = self.segments[self.cur_seq]
                if s then
                        context:music(s[1], s[2], s[3])
                end
        end
end

-- A dust cloud is generated at the player's feet when they hit the ground hard
-- in any direction. For this scenery, the dust cloud persists in place and
-- dies after a number of frames

dust = {
        sprite_sheet = {
                {{-4,-4,0x108}},
                {{-4,-4,0x109}},
                {{-4,-4,0x10A}}
        },
        frames = {1,1,1,2,2,2,3,3,3}
}

dust.__index = dust

function dust:create()
        self.state = 0
end

function dust:update()
        self.state = self.state + 1
        if self.state > 9 then
                return true
        else
                context:sprites(self.box, 0, self.sprite_sheet[self.frames[self.state]])
        end
end

-- Radios are a test for a shared state. The current channel is set in the
-- radio object, and all mobs share it. When the player intracts with any of
-- them, the channel changes.

radio = {
        sprite_sheet = {{-4,-4,0x140}},
        channel = 0
}

radio.__index = radio

function radio:create()
        self.flags = 0
end

function radio:focus()
        self.flags = self.flags | sprite_focus;
end

function radio:blur()
        self.flags = self.flags & ~sprite_focus;
end

function radio:trigger()
        self.channel = self.channel + 1
        context:say(rando.dialogue[self.channel])
        if self.channel == #rando.dialogue then
                self.channel = 0
        end
end

function radio:update()
        context:sprites(self.box, self.flags, self.sprite_sheet)
end

-- Invisible object that plays the Fall music

fall_soundtrack = {}

fall_soundtrack.__index = fall_soundtrack

function fall_soundtrack:create()
        audio_queues:transition("fall")
end

function fall_soundtrack:update()
        audio_queues:update()
end

-- Invisible object that plays the Zwosta street music, with no transition
-- because this is the base music.

zwosta_soundtrack = {}

zwosta_soundtrack.__index = zwosta_soundtrack

function zwosta_soundtrack:create()
        audio_queues:transition("zwosta")
end

function zwosta_soundtrack:update()
        audio_queues:update()
end

-- Invisible object that plays the danger music, with a transition from and to
-- the zwosta street music.

tunnel_soundtrack = {}

tunnel_soundtrack.__index = tunnel_soundtrack

function tunnel_soundtrack:create()
        audio_queues:transition("tunnel")
end

function tunnel_soundtrack:update()
        audio_queues:update()
end

-- Invisible object that plays Iorena's music, with a transition from and to
-- the Zwosta street music

office_soundtrack = {}

office_soundtrack.__index = office_soundtrack

function office_soundtrack:create()
        audio_queues:transition("office")
end

function office_soundtrack:update()
        audio_queues:update()
end

-- Clocks are a test for persistent state. They hold a bottle of aqvavit, and
-- the first time the player interacts with them they get it; but once this
-- happens, the mob's persistent flags are changed to reflect this.

clock = {
        sprite_sheet = {{-4,-12,0x141},{-4,-4,0x142},{-4,4,0x143}}
}

clock.__index = clock

function clock:create()
        self.flags = 0
end

function clock:focus()
        self.flags = self.flags | sprite_focus
end

function clock:blur()
        self.flags = self.flags & ~sprite_focus
end

function clock:trigger()
        if persistent[self.id] then
                context:say("Years ago there was an accident with a\n" ..
                            "truck full of these; these days, most\n" ..
                            "houses have one.")
        else
                context:sound(7)
                context:say("Got a bottle of aqvavit")
                persistent[self.id] = true
        end
end

function clock:update()
        context:sprites(self.box, self.flags, self.sprite_sheet)
end

-- The wall clock is just an item that can be interacted.

wall_clock = {
        sprite_sheet = {{-4,-8,0x141},{-4,0,0x144}}
}

wall_clock.__index = wall_clock

function wall_clock:create()
        self.flags = 0
end

function wall_clock:focus()
        self.flags = self.flags | sprite_focus
end

function wall_clock:blur()
        self.flags = self.flags & ~sprite_focus
end

function wall_clock:trigger()
        context:say("Huh. This one's different from the rest");
end

function wall_clock:update()
        context:sprites(self.box, self.flags, self.sprite_sheet)
end

-- Test for distortion layer

distortion = { }

distortion.__index = distortion

function distortion:create()
        self.particles = {
                {x=0,y=0,s=0},
                {x=0,y=-40,s=10}
        }
end

function distortion:update()
        for _,p in ipairs(self.particles) do
                if p.s == 0 then
                        p.x = 0
                        p.y = 0
                        p.s = 30
                end
                p.y = p.y - 2
                p.s = p.s - 1
                context:distortions(self.box, 0, {{p.x - p.s, p.x + p.s, p.y - p.s, p.y + p.s}})
        end
end

-- Radios are a test for a shared state. The current channel is set in the
-- radio object, and all mobs share it. When the player intracts with any of
-- them, the channel changes.

television = {
        messages = {
                "Demonstrations on the aniversary of the Bombing of Nuskom were met with force from local police.",
                "The Rigasian Government has published a communique condemning the rioters and supporting the Mesogeian Administration.",
                "Are you psychic? Have strange skills? The Paranormal Foundation in Rigasia is interested. Always hiring.",
        },
        sprite_sheet = {{-8,-8,0x147},{0,-8,0x148},{-8,0,0x167},{0,0,0x168}},
        state = 0
}

television.__index = television

function television:create()
        self.flags = 0
end

function television:focus()
        self.flags = self.flags | sprite_focus
end

function television:blur()
        self.flags = self.flags & ~sprite_focus
end

function television:trigger()
        self.state = self.state + 1
        context:say(self.messages[self.state])
        if self.state == #self.messages then
                self.state = 0
        end
end

function television:update()
        context:sprites(self.box, self.flags, self.sprite_sheet)
end

-- A small computer object

computer = {
        sprite_sheet = {{-4,-4,0x145}}
}

computer.__index = computer

function computer:create()
        self.flags = 0
end

function computer:focus()
        self.flags = self.flags | sprite_focus
end

function computer:blur()
        self.flags = self.flags & ~sprite_focus
end

function computer:update()
        context:sprites(self.box, self.flags, self.sprite_sheet)
end

-- A small computer object

iorenas_computer = {
        sprite_sheet = {{-4,-4,0x145}}
}

iorenas_computer.__index = iorenas_computer

function iorenas_computer:create()
        self.flags = 0
end

function iorenas_computer:focus()
        self.flags = self.flags | sprite_focus
end

function iorenas_computer:blur()
        self.flags = self.flags & ~sprite_focus
end

function iorenas_computer:update()
        context:sprites(self.box, self.flags, self.sprite_sheet)
end

function iorenas_computer:trigger()
        context:say("I don't know the password, and even if\n" ..
                    "I did, she would hurt me if I touched\n" ..
                    "her things.")
end

-- A door that opens (dissappears) when used

door = {
        sprite_sheet = {{-4,-12,0x162},{-4,-4,0x182},{-4,4,0x162}}
}

door.__index = door

function door:create()
        if persistent[self.id] then 
                return true
        else
                self.flags = 0
                context:hitmask(to_cellsb(self.box), 1)
        end
end

function door:focus()
        self.flags = self.flags | sprite_focus
end

function door:blur()
        self.flags = self.flags & ~sprite_focus
end

function door:update()
        context:sprites(self.box, self.flags, self.sprite_sheet)
end

function door:trigger()
        context:sound(1)
        context:hitmask(to_cellsb(self.box), 0)
        persistent[self.id] = true
        return true
end

-- A fly is a flying mob that buzzes around and then dives for the player

fly = {}

fly.__index = fly

function fly:create()
        self.vel = pointsv(0, 0)
        self.flags = 0
        self.awake = false
end

function fly:focus()
        self.flags = self.flags | sprite_focus
end

function fly:blur()
        self.flags = self.flags & ~sprite_focus
end

function fly:update()
        if not self.awake then
                self.awake = self.box:intersects(to_pointsb(context:screen()))
                return
        end
        if self.frame == 0x1A0 then
                self.frame = 0x1A1
        else
                self.frame = 0x1A0
        end
        delta = context:pc_box():center() - self.box:center()
        self.vel = (self.vel + delta / 100):clamp(pointsv(32,32))
        self.box = self.box + self.vel;
        if delta.x:get() < 0 then
                self.flags = self.flags | sprite_flipx
        else
                self.flags = self.flags & ~sprite_flipx
        end
        context:sprites(self.box, self.flags, {{-4,-4,self.frame}})
        if self.box:intersects(context:pc_attack()) then
                context:sound(6)
                context:make_mob("splatter", self.box);
                return true
        elseif self.box:intersects(context:pc_box()) then
                context:pc_harm(self.box)
        end
end

-- A splatter is a particle with splattering gibs animation

splatter = {
        sprite_sheet = {
                {{-8, -8, 0x340}, {0, -8, 0x8340}, {-8, 0, 0x4340}, {0, 0, 0xC340}},
                {{-8, -8, 0x341}, {0, -8, 0x4A1}, {-8, 0, 0x8A1}, {0, 0, 0x36A1}},
                {{-8, -8, 0x342}, {0, -8, 0x343}, {-8, 0, 0x362}, {0, 0, 0x363}},
                {{-8, -8, 0x344}, {0, -8, 0x345}, {-8, 0, 0x364}, {0, 0, 0x365}},
                {{-8, -8, 0x346}, {0, -8, 0x347}, {-8, 0, 0x366}, {0, 0, 0x367}}
        },
        frames = {1, 2, 3, 3, 4, 4, 5, 5}
}

splatter.__index = splatter

function splatter:create()
        self.state = 0
end

function splatter:update()
        self.state = self.state + 1
        if self.state > 8 then
                return true
        else
                context:sprites(self.box, 0, self.sprite_sheet[self.frames[self.state]])
        end
end

-- Sign in the starting well room

well_sign = {
        sprite_sheet = {
                {-12,-8,0x163},{-4,-8,0x164},{4,-8,0x8163},
                {-12,0,0x4163},{-4,0,0x4164},{4,0,0xC163}
        }
}

well_sign.__index = well_sign

function well_sign:create()
        self.flags = 0
end

function well_sign:focus()
        self.flags = self.flags | sprite_focus
end

function well_sign:blur()
        self.flags = self.flags & ~sprite_focus
end

function well_sign:trigger()
        context:say("West: Zwosta Trail\n" ..
                    "East: Nuskom Exclusion Zone\n" ..
                    "DO NOT ENTER")
end

function well_sign:update()
        context:sprites(self.box, self.flags, self.sprite_sheet)
end

-- Sign in the starting well room

flywell_sign = {
        sprite_sheet = {
                {-12,-8,0x163},{-4,-8,0x164},{4,-8,0x8163},
                {-12,0,0x4163},{-4,0,0x4164},{4,0,0xC163}
        }
}

flywell_sign.__index = flywell_sign

function flywell_sign:create()
        self.flags = 0
end

function flywell_sign:focus()
        self.flags = self.flags | sprite_focus
end

function flywell_sign:blur()
        self.flags = self.flags & ~sprite_focus
end

function flywell_sign:trigger()
        context:say("West: Route 4\n" ..
                    "East: Zwosta\n" ..
                    "Down: Nuskom\n")
end

function flywell_sign:update()
        context:sprites(self.box, self.flags, self.sprite_sheet)
end

-- A rabbit is a rabbiting mob that buzzes around and then dives for the player

rabbit = {
        sprite_sheet = {
                {{-8,-4,0x1A2},{0,-4,0x1A3}},
                {{-8,-4,0x1A4},{0,-4,0x1A5}},
                {{-8,-4,0x1A6},{0,-4,0x1A7}},
                {{-8,-4,0x1A8},{0,-4,0x1A9}},
                {{-8,-4,0x1AA},{0,-4,0x1AB}},
        },
        frames = {1,2,2,2,3,3,3,4,4,4,5}
}

rabbit.__index = rabbit

function rabbit:create()
        self.vel = pointsv(0, 0)
        self.contact = boolb(false, false, false, false)
        self.frame = 1
        self.flags = 0
        self.awake = false
end

function rabbit:focus()
        self.flags = self.flags | sprite_focus
end

function rabbit:blur()
        self.flags = self.flags & ~sprite_focus
end

function rabbit:update()
        delta = context:pc_box():center() - self.box:center()
        if math.abs(delta.x:get()) < 512 then
                self.awake = true
        end
        if self.awake then
                if not self.contact.d then
                        self.vel = self.vel + pointsv(0,8)
                elseif delta.x:get() > 0 then
                        self.vel = self.vel + pointsv(2,0)
                else
                        self.vel = self.vel + pointsv(-2,0)
                end
                self.vel = self.vel:clamp(pointsv(32,32))
                self.box, self.contact = context:move_mob(self.box, self.vel);
                if delta.x:get() * self.vel.x:get() < 0 then
                        self.frame = 11
                        if self.contact.d and math.random() < 0.5 then
                                context:make_mob("dust", pointsb(0, 0, 0, 0) + self.box:dn());
                        end
                else
                        self.frame = self.frame + 1
                        if self.frame >= 11 then
                                self.frame = 2
                        end
                end
                if delta.x:get() < 0 then
                        self.flags = self.flags | sprite_flipx
                else
                        self.flags = self.flags & ~sprite_flipx
                end
        else
                self.frame = 1
        end
        context:sprites(self.box, self.flags, self.sprite_sheet[self.frames[self.frame]])
        if self.box:intersects(context:pc_attack()) then
                context:sound(6)
                context:make_mob("splatter", self.box);
                return true
        elseif self.box:intersects(context:pc_box()) then
                context:pc_harm(self.box)
        end
end

-- A bear is a bearing mob that buzzes around and then dives for the player

bear = {
        sprite_sheet = {
                {{-16,-8,0x1C0},{-8,-8,0x1C1},{0,-8,0x1C2},
                 {-16,0,0x1E0},{-8,0,0x1E1},{0,0,0x1E2},{6,-3,0x1C7}},
                {{-16,-8,0x1C0},{-8,-8,0x1C1},{0,-8,0x1C2},
                 {-16,0,0x1E0},{-8,0,0x1E1},{0,0,0x1E2},{6,-3,0x1C7}},
                {{-16,-8,0x1C4},{-8,-8,0x1C5},{0,-8,0x1C6},{8,-8,0x01C7},
                 {-16,0,0x1E4},{-8,0,0x1E5},{0,0,0x1E6},{8,0,0x1E7}},
                {{-16,-8,0x1C8},{-8,-8,0x1C9},{0,-8,0x1CA},
                 {-16,0,0x1E8},{-8,0,0x1E9},{0,0,0x1EA},{7,-6,0x1C7}},
                {{-16,-8,0x1CB},{-8,-8,0x1CC},{0,-8,0x1CD},
                 {-16,0,0x1EB},{-8,0,0x1EC},{0,0,0x1ED}},
        },
        frames = {1,2,2,2,2,3,3,3,3,4,4,4,4,5}
}

bear.__index = bear

function bear:create()
        self.vel = pointsv(0, 0)
        self.contact = boolb(false, false, false, false)
        self.frame = 1
        self.flags = 0
        self.awake = false
end

function bear:focus()
        self.flags = self.flags | sprite_focus
end

function bear:blur()
        self.flags = self.flags & ~sprite_focus
end

function bear:update()
        delta = context:pc_box():center() - self.box:center()
        if math.abs(delta.x:get()) < 512 then
                self.awake = true
        end
        if self.awake then
                if not self.contact.d then
                        self.vel = self.vel + pointsv(0,8)
                elseif delta.x:get() > 0 then
                        self.vel = self.vel + pointsv(1,0)
                else
                        self.vel = self.vel + pointsv(-1,0)
                end
                self.vel = self.vel:clamp(pointsv(32,32))
                self.box, self.contact = context:move_mob(self.box, self.vel);
                if delta.x:get() * self.vel.x:get() < 0 then
                        self.frame = 14
                        if self.contact.d and math.random() < 0.5 then
                                context:make_mob("dust", pointsb(0, 0, 0, 0) + self.box:dn());
                        end
                else
                        self.frame = self.frame + 1
                        if self.frame >= 14 then
                                self.frame = 2
                        end
                end
                if delta.x:get() < 0 then
                        self.flags = self.flags | sprite_flipx
                else
                        self.flags = self.flags & ~sprite_flipx
                end
        else
                self.frame = 1
        end
        context:sprites(self.box, self.flags, self.sprite_sheet[self.frames[self.frame]])
        if self.box:intersects(context:pc_attack()) then
                context:sound(9)
        elseif self.box:intersects(context:pc_box()) then
                context:pc_harm(self.box)
        end
end

-- Randos are random NPCs in zwosta

rando = {
        dialogue = {
                "Don't point your weapon at me!",
                "Sigh. Times are tough.",
                "Welcome to Zwosta!",
                "I'unno.",
                "Good morning!",
                "If we had the guts, we'd face Rigasia regardless of how many bombs they may have.",
                "Country X is getting cocky, they should be careful to not get bombed too.",
                "Today it's X years since my Y died in the bombing, and I don't get time to even visit the graveyard.",
                "They say that yet another convoy with doctors and tech stuff has entered The Zone.",
                "We should stop sending people to The Zone, and just cover it in concrete.",
                "I wish I was psychic. The Foundation would pay enough to never have to work again.",
                "I lost again at dice! what the hell? That luck is not natural. It may be a psychic.",
                "You're one of Iorena's kids, aren't you? You should be careful: what she makes you do is not right.",
                "Is that scrap because of an aberration? Oh god, don't get near me. That thing may be contagious.",
                "Oh, hey there. Say hello to Iorena for me, okay?",
                "Blond. Short. Angry. Got caught in that asshole's lies about the old glories of nuskom and is now dead in a ditch.",
                "Look, it may be a broken ruin of a city, but Nuskom's skyline is still awesome.",
                "I'm soooo fucking tired of work, but I'm still going to <bar> when i'm done. Even if I end passing out on a bench.",
                "So winter came and went, and the lake is melting, and there are bugs everywhere. Hate them.",
                "Well, it's not cold enough to freeze car engines anymore, so that's good news!",
                "You're one of Iorena's? Does she tell you to go into The Zone too?",
                "Some people tried to bring contraband through The Zone and it mutated! Don't buy if you don't know where it's been.",
                "I don't care what the militia says: food doesn't mutate. I don't earn enough to buy legit, anyway.",
                "I can't wait to see the next episode of <insert show here>",
                "Looking at the light side, it has been like two decades without being bombed. I guess.",
                "Damn, you're young. Were you even alive when the Bombing?",
                "You know why it's the P-bomb? It means 'pyra' in Gotthos, that is, portal.",
                "They use the same shit they used for the bomb to open rips to other worlds. That can't be good.",
                "I was thinking of going through a gate on Rigasia; things can't be worse on the other side.",
                "I don't care how they try to sell it: if gates went somewhere good, they'd be taken by rich assholes.",
                "Good thing about gates is that the Empire is too busy fucking the people on the other side to fuck with us.",
                "I wonder what's different between the bubbles in The Zone and gates. Bubbles kill, gates travel? how?",
                "Do you think Rigasia will ever stop expanding? I mean, aren't there infinite worlds through the gates?",
                "Somewhere on the other side of the gates there's someone who will fuck the Empire; I want to live to see that.",
                "Fuck Mesogeia and their police: my friend was gassed on the demonstrations and has permanent damage.",
                "As long as fuckups like the Bombing riots keep happening, Rigasia will keep vetoing free elections here.",
        }
}

rando.__index = rando

function rando:create()
        if math.random() > 0.25 then
                return true
        else
                self.flags = 0
                self.sprite_sheet = {{-4,-10,math.random(0x220,0x23F)},
                                     {-4,-2,math.random(0x200,0x20D)}}
                self.dialogue = self.dialogue[math.random(#self.dialogue)];
                self.flip = math.random(0,1)
                self.time = 0
        end
end

function rando:focus()
        self.flags = self.flags | sprite_focus
end

function rando:blur()
        self.flags = self.flags & ~sprite_focus
end

function rando:trigger()
        context:say(self.dialogue)
end

function rando:update()
        if self.time == 0 then
                self.time = 15
                delta = context:pc_box():center() - self.box:center()
                if math.abs(delta.x:get()) > 512 then
                        if self.flip then
                                self.flags = self.flags | sprite_flipx
                        else
                                self.flags = self.flags & ~sprite_flipx
                        end
                else
                        if delta.x:get() < 0 then
                                self.flags = self.flags | sprite_flipx
                        else
                                self.flags = self.flags & ~sprite_flipx
                        end
                end
        end
        self.time = self.time - 1
        context:sprites(self.box, self.flags, self.sprite_sheet)
end

