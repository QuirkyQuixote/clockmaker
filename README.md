Clockmaker - A very retro RPG
=============================

Every decade or so I retake this project and see how I would implement it in
the present.  The result is going to be very retro.

- graphics are limited to two layers: a tile layer and a sprite layer,
- tiles and sprites are limited to 1024 8x8 pixel bitmasks.
- one of the sprite colors is transparent,
- input is limited to four directions and four buttons,
- scrolling is limited to one direction.

Installation
------------

The code is written in 2020 ISO C++.  It uses the libraries `stdc++`, `GL`,
`GLM`, `GLEW`, `SDL2`, `SDL2_image`, and `Lua5.4`:
It has been tested natively in Debian 12, and successfully cross-compiled for
Windows.

```shell
sudo apt install build-essential
sudo apt install libsdl2-dev libsdl2-image-dev
sudo apt install libgl1-mesa-dev libglm-dev libglew-dev
sudo apt install liblua5.4-dev
```

By default, the make scripts installs everything in `prefix=$(HOME)/.local`,
with the executable in `bindir=$(prefix)/bin` and the data in
`datadir=$(prefix)/share/clockmaker`; this can be changed by providing
alternative values for `prefix`, `bindir`, or `datadir` to make, but note below
about the paths where the executable searches for data.

```shell
make all install
```

Launch with `clockmaker`

The executable searches for data in a `clockmaker` directory on paths
according to the XDG base directory specification, in order:

- wherever `$XDG_DATA_HOME` says, or if not defined `$HOME/.local/share`;
- wherever `$XDG_DATA_DIRS` says, or if not defined `/usr/local/share:/usr/share`.

Resource Format
---------------

The graphic set for each layer is stored in a GIF file, 256x256 pixels; this
results in 32x32=1024 bitmasks.

The current scene is a JSON file created in tiled with three layers named
"collision", "objects", and "tiles":

- collision defines the collision mask for physics: every tile that is not
  unset is considered solid; every one that is, is considered empty.

- objects is an object layer that contains rectangles. The only properties that
  matter are their names and the boxes; these declare mobs, rooms, and the
  princess herself.

- tiles is the background layer and is loaded verbatim into the tile layer.

