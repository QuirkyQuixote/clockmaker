// game.h - declare game logic

// Copyright (C) 2022 L. Sanz <luis.sanz@gmail.com>

// This file is part of Clockmaker.

// Clockmaker is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Clockmaker is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Clockmaker.  If not, see <https://www.gnu.org/licenses/>.


#ifndef SRC_GAME_H_
#define SRC_GAME_H_

#include "context.h"
#include "mixer.h"

// Handles audio for the game
class Audio {
 private:
        Subsystem_lock<SDL_INIT_AUDIO> slock;
        Mixer mixer;
        std::vector<size_t> sounds;
        size_t music;

 public:
        Audio(const Audio_config& config);
        ~Audio();
        void update(Context& context);
};

// Follows the princess
class Camera {
 private:
        bool scrolling = true;
        pixels16v offset{0, 0};

        void inside(Context& context);
        void outside(Context& context);
        pixels16v recalculate_offset(Context& context);

 public:
        enum class State { outside, inside, enter, exit, };

        Camera(Context& context);
        State update(Context& context);
};

// Manages user input
class Event_handler {
 private:
        Subsystem_lock<SDL_INIT_GAMECONTROLLER> slock;
        const Input_config& config;
        std::vector<Gamepad> gamepads;

        void handle_event(const SDL_Event& event, Input& input);

 public:
        Event_handler(const Input_config& config);
        void update(Input& input);
};

// This is called every frame to update the general state
class Update : public Main_loop<Update> {
 private:
        Context& context;
        Audio audio;
        Camera camera;
        Renderer renderer;
        Event_handler event_handler;
        points32b level_spawn;
        points32b room_spawn;

        void handle_event(SDL_Event& event);
        void enter_room();
        void inside_room();
        void exit_room();
        void outside_room();

 public:
        Update(const Config& config, Context& context);
        bool update();
};

#endif // SRC_GAME_H_
