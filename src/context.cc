// context.cc - load and save context

// Copyright (C) 2022 L. Sanz <luis.sanz@gmail.com>

// This file is part of Clockmaker.

// Clockmaker is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Clockmaker is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Clockmaker.  If not, see <https://www.gnu.org/licenses/>.


#include "context.h"

#include <fstream>

#include "xdgbds.h"
#include "mdea/mdeaxx.h"

#include "media.h"

class Context_loader {
 private:
        Context context;
        std::vector<Mob_place> places;

 public:
        Context operator()(const std::filesystem::path& path)
        {
                if (auto doc = mdea::document{path}) {
                        parse(doc.root().get_object());
                        return std::move(context);
                }
                throw std::runtime_error{path.string() + ": bad JSON"};
        }

 private:
        void parse(const mdea::object& obj)
        {
                parse_layers(obj["layers"].get_array());
                parse_tilesets(obj["tilesets"].get_array());
                parse_places();
        }

        void parse_places()
        {
                for (auto& room : context.rooms) {
                        room.first_place = context.places.size();
                        for (auto& place : places)
                                if (geom::contains(room.box, place.box))
                                        context.places.push_back(place);
                        room.last_place = context.places.size();
                }
        }

        void parse_layers(const mdea::array& array)
        {
                for (auto x : array)
                        parse_layer(x.get_object());
        }

        void parse_layer(const mdea::object& obj)
        {
                auto name = obj["name"].get_string();
                if (name == "tiles")
                        parse_tile_layer(obj);
                else if (name == "collision")
                        parse_collision_layer(obj);
                else if (name == "objects")
                        parse_object_layer(obj);
        }

        void parse_tile_layer(const mdea::object& obj)
        {
                cells16 w(obj["width"].get_long());
                cells16 h(obj["height"].get_long());
                context.nametable.resize({}, cells16v{w, h});
                auto it = context.nametable.begin();
                for (auto v : obj["data"].get_array()) {
                        auto x = v.get_long();
                        uint16_t y = static_cast<uint16_t>(x - 1);
                        if (x & 0x80000000) y |= Sprite_flags::fliph;
                        if (x & 0x40000000) y |= Sprite_flags::flipv;
                        if (x & 0x20000000) y |= Sprite_flags::flipd;
                        *it++ = y;
                }
        }

        void parse_collision_layer(const mdea::object& obj)
        {
                cells16 w(obj["width"].get_long());
                cells16 h(obj["height"].get_long());
                context.hitmask.resize({}, cells16v{w, h});
                auto it = context.hitmask.begin();
                for (auto v : obj["data"].get_array())
                        *it++ = (v.get_long() > 0);
        }

        void parse_tilesets(const mdea::array& array)
        {
                for (auto v : array)
                        parse_tileset(v.get_object());
        }

        void parse_tileset(const mdea::object& obj)
        {
                //std::filesystem::path image{obj["image"].get_string()};
                //context.tiles.texture = load_image(xdg::data::find(BASEDIR / image));
        }

        void parse_object_layer(const mdea::object& obj)
        {
                for (auto v : obj["objects"].get_array())
                        parse_object(v.get_object());
        }

        void parse_object(const mdea::object& obj)
        {
                int id = obj["id"].get_long();
                pixels16 x(obj["x"].get_long());
                pixels16 y(obj["y"].get_long());
                pixels16 w(obj["width"].get_long());
                pixels16 h(obj["height"].get_long());
                points32b box(x, x + w, y, y + h);
                auto type = obj["type"].get_string();
                auto name = obj["name"].get_string();
                if (type == "princess")
                        context.princess.body.box = box;
                else if (type == "dummy")
                        places.emplace_back(id, box, std::string(name));
                else if (type == "room")
                        context.rooms.push_back({box, 0, 0});
                else
                        throw std::runtime_error{"bad mob type: " + std::string(type)};
        }
};

Context load_context(const std::filesystem::path& path)
{
        Context_loader load;
        return load(path);
}
