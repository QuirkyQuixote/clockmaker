// mobs.cc - game logic for movable objects

// Copyright (C) 2022 L. Sanz <luis.sanz@gmail.com>

// This file is part of Clockmaker.

// Clockmaker is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Clockmaker is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Clockmaker.  If not, see <https://www.gnu.org/licenses/>.


#include "mobs.h"

#include "geom/table.h"

#include "render.h"
#include "physics.h"
#include "context.h"

// Use RAII to ensure the Lua stack is left in the right state.
struct Stack_guard {
        lua_State* state;
        int top;
        Stack_guard(lua_State* state) : state{state}, top{lua_gettop(state)} {}
        ~Stack_guard() { lua_pop(state, lua_gettop(state) - top); }
};

// Create Lua object for a mob
Mob::Mob(lua_State* lua_state, const std::string& name, int id, const points32b& box)
        : lua_state{lua_state}
{
        Stack_guard _{lua_state};
        lua_createtable(lua_state, 0, 0);
        if (lua_getglobal(lua_state, name.c_str()) != LUA_TTABLE) {
                std::stringstream message;
                message << "No metatable called [" << name << "]";
                throw std::runtime_error{message.str()};
        }
        lua_setmetatable(lua_state, -2);
        ishtar::push(lua_state, id);
        lua_setfield(lua_state, -2, "id");
        ishtar::push(lua_state, box);
        lua_setfield(lua_state, -2, "box");
        lua_ref = luaL_ref(lua_state, LUA_REGISTRYINDEX);
        event.set(mf_create);
}

// Trigger an event on a mob
bool Mob::call_(const char* event)
{
        Stack_guard _{lua_state};
        if (lua_rawgeti(lua_state, LUA_REGISTRYINDEX, lua_ref) != LUA_TTABLE)
                throw std::runtime_error{"Can't find Lua object for mob"};
        if (lua_getfield(lua_state, -1, event) != LUA_TFUNCTION)
                return false; // can be ignored
        lua_pushvalue(lua_state, -2);
        if (lua_pcall(lua_state, 1, 1, 0) != LUA_OK)
                throw std::runtime_error{lua_tostring(lua_state, -1)};
        return ishtar::to<bool>(lua_state, -1);
}

// Get field for mob
template<class T> std::optional<T> Mob::get_(const char* name) const
{
        Stack_guard _{lua_state};
        if (lua_rawgeti(lua_state, LUA_REGISTRYINDEX, lua_ref) != LUA_TTABLE)
                throw std::runtime_error{"Can't find Lua object for mob"};
        if (lua_getfield(lua_state, -1, name) != LUA_TNIL)
                return ishtar::to<T>(lua_state, -1);
        return std::nullopt;
}

// Call the right update function for a given mob
bool Mob::update(Context& ct)
{
        if (event.test(mf_create))
                if (call_("create")) return true;
        auto prev_event = event;
        event = 0;
        if (auto box = get_<points32b>("box")) {
                if (geom::intersects(box.value(), ct.princess.target_box))
                        event.set(mf_focus);
                if (geom::intersects(box.value(), ct.princess.attack_box))
                        event.set(mf_trigger);
                if (event.test(mf_focus) && !prev_event.test(mf_focus))
                        if (call_("focus")) return true;
                if (!event.test(mf_focus) && prev_event.test(mf_focus))
                        if (call_("blur")) return true;
                if (event.test(mf_trigger) && !prev_event.test(mf_trigger))
                        if (call_("trigger")) return true;
        }
        return call_("update");
}

void clear_mobs(Context& ct)
{
        ct.mobs.clear();
}

void update_mobs(Context& ct)
{
        auto pred = [&ct](Mob& mob){ return mob.update(ct); };
        auto new_end = std::remove_if(ct.mobs.begin(), ct.mobs.end(), pred);
        ct.mobs.erase(new_end, ct.mobs.end());
}

void load_mobs(Context& ct)
{
        for (auto i = ct.room.first_place; i != ct.room.last_place; ++i) {
                auto& place = ct.places[i];
                ct.mobs.emplace_back(ct.lua_state, place.name, place.id, place.box);
        }
}

Mob& make_mob(Context& ct, const std::string& name, const points32b& box)
{
        ct.mobs.emplace_back(ct.lua_state, name, -1, box);
        return ct.mobs.back();
}

