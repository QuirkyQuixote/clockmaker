// render.cc - instructions for scene rendering

// Copyright (C) 2022 L. Sanz <luis.sanz@gmail.com>

// This file is part of Clockmaker.

// Clockmaker is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Clockmaker is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Clockmaker.  If not, see <https://www.gnu.org/licenses/>.


#include "render.h"

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/type_ptr.hpp"

#include "context.h"

// A Quad is a geometry set that contains the vertices for a rectangle covering
// exactly the screen. Used to post-process textures.

Quad::Quad()
{
        static const float vertices[] = {
                -1.0f,  1.0f,  0.0f, 1.0f,
                -1.0f, -1.0f,  0.0f, 0.0f,
                1.0f, -1.0f,  1.0f, 0.0f,
                -1.0f,  1.0f,  0.0f, 1.0f,
                1.0f, -1.0f,  1.0f, 0.0f,
                1.0f,  1.0f,  1.0f, 1.0f
        };
        glBindVertexArray(*vao);
        glBindBuffer(GL_ARRAY_BUFFER, *vbo);
        glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), &vertices, GL_STATIC_DRAW);
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*)0);
        glEnableVertexAttribArray(1);
        glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*)(2 * sizeof(float)));
}

void Quad::render()
{
        glBindVertexArray(*vao);
        glDrawArrays(GL_TRIANGLES, 0, 6);
}

Geometry::Geometry()
{
        glBindVertexArray(*vao_);
        glBindBuffer(GL_ARRAY_BUFFER, *vbo_);
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*) 0);
        glEnableVertexAttribArray(1);
        glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*) offsetof(Vertex, u));
        glEnableVertexAttribArray(2);
        glVertexAttribPointer(2, 1, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*) offsetof(Vertex, s));
}

void Geometry::update(const Vertex* buf, size_t len)
{
        glBindBuffer(GL_ARRAY_BUFFER, *vbo_);
        glBufferData(GL_ARRAY_BUFFER, len * sizeof(Vertex), buf, GL_STATIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        size_ = len;
}

void Geometry::render()
{
        glBindVertexArray(*vao_);
        glDrawArrays(GL_TRIANGLES, 0, size_);
}

void Tile_layer::update(const Nametable& nametable)
{
        std::vector<Vertex> buf;
        auto begin = nametable.begin();
        auto end = nametable.end();
        for (auto it = begin; it != end; ++it)
                push_vertices(buf, {geom::path(begin, it), *it});
        geometry.update(buf.data(), buf.size());
}

void Tile_layer::render()
{
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, *texture);
        geometry.render();
}

void Sprite_layer::update(const std::vector<Sprite>& sprites)
{
        std::vector<Vertex> buf;
        for (auto& s : sprites)
                push_vertices(buf, s);
        geometry.update(buf.data(), buf.size());
}

void Sprite_layer::render()
{
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, *texture);
        geometry.render();
}

void push_vertices(std::vector<Vertex>& buf, Sprite s)
{
        int u = s.c % 32;
        int v = (s.c / 32) % 32;
        GLfloat x0 = (int)s.p.x;
        GLfloat y0 = (int)s.p.y;
        GLfloat x1 = x0 + 8;
        GLfloat y1 = y0 + 8;
        GLfloat u0 = u / 32.0;
        GLfloat v0 = v / 32.0;
        GLfloat u1 = (u + 1) / 32.0;
        GLfloat v1 = (v + 1) / 32.0;
        GLfloat sp = 0.0;
        if ((s.c & Sprite_flags::focus)) {
                sp = SDL_GetTicks() * 0.002;
                sp = (ceil(sp) - sp) * 0.5;
        }
        Vertex data[4] = {
                {x0, y0, u0, v0, sp},
                {x1, y0, u1, v0, sp},
                {x0, y1, u0, v1, sp},
                {x1, y1, u1, v1, sp}
        };
        if ((s.c & Sprite_flags::flipd)) {
                std::swap(data[1].u, data[2].u);
                std::swap(data[1].v, data[2].v);
        }
        if ((s.c & Sprite_flags::fliph)) {
                std::swap(data[0].u, data[1].u);
                std::swap(data[0].v, data[1].v);
                std::swap(data[2].u, data[3].u);
                std::swap(data[2].v, data[3].v);
        }
        if ((s.c & Sprite_flags::flipv)) {
                std::swap(data[0].u, data[2].u);
                std::swap(data[0].v, data[2].v);
                std::swap(data[1].u, data[3].u);
                std::swap(data[1].v, data[3].v);
        }
        buf.push_back(data[0]);
        buf.push_back(data[1]);
        buf.push_back(data[3]);
        buf.push_back(data[0]);
        buf.push_back(data[3]);
        buf.push_back(data[2]);
}

void Distortion_layer::update(const std::vector<Distortion>& distortions)
{
        std::vector<Vertex> buf;
        for (auto& s : distortions)
                push_vertices(buf, s);
        geometry.update(buf.data(), buf.size());
}

void Distortion_layer::render()
{
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, *texture);
        geometry.render();
}

void push_vertices(std::vector<Vertex>& buf, Distortion d)
{
        GLfloat x0 = (int)d.box.l;
        GLfloat y0 = (int)d.box.d;
        GLfloat x1 = (int)d.box.r;
        GLfloat y1 = (int)d.box.u;
        GLfloat u0 = 0;
        GLfloat v0 = 0;
        GLfloat u1 = 1;
        GLfloat v1 = 1;
        buf.push_back({x0, y0, u0, v0});
        buf.push_back({x1, y0, u1, v0});
        buf.push_back({x1, y1, u1, v1});
        buf.push_back({x0, y0, u0, v0});
        buf.push_back({x1, y1, u1, v1});
        buf.push_back({x0, y1, u0, v1});
}

// Manages high-level rendering logic, so basically it loads a pair of shaders
// as specified in the configuration, holds the tile and sprite layers, and
// calculates the MVP matrix.
//
// The update() function is called to rebuild geometry. The sprite geometry
// will always be rebuilt; the tile geometry will only be rebuild if
// context.update_nametable is true.
//
// The render() function renders the geometry that is built at that moment

Geometry_renderer::Geometry_renderer(const Render_config& config) :
        program{vertex_shader(xdg::data::find(BASEDIR "/shaders/geometry.vert")),
                fragment_shader(xdg::data::find(BASEDIR "/shaders/geometry.frag"))},
        program_mvp(program, "mvp"),
        program_image(program, "image")
{
        auto w = (int)pixels16(viewport.x);
        auto h = (int)pixels16(viewport.y);
        auto filter = config.blur ? GL_LINEAR : GL_NEAREST;

        glBindFramebuffer(GL_FRAMEBUFFER, *fbo);
        glBindTexture(GL_TEXTURE_2D, *color_buffer);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, w, h, 0, GL_RGBA, GL_FLOAT, NULL);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, filter);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, filter);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, *color_buffer, 0);

        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
                throw std::runtime_error{"Framebuffer is not complete!"};
}

void Geometry_renderer::update(Context& context)
{
        if (context.update_nametable) {
                tiles.update(context.nametable);
                context.update_nametable = false;
        }
        sprites.update(context.sprites);
}

const Texture& Geometry_renderer::render(Context& context)
{
        glBindFramebuffer(GL_FRAMEBUFFER, *fbo);
        glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        glClear(GL_COLOR_BUFFER_BIT);
        glDisable(GL_CULL_FACE);
        glDisable(GL_DEPTH_TEST);

        glViewport(0, 0, (int)pixels16(viewport.x), (int)pixels16(viewport.y));
        pixels16b box{context.offset, context.offset + viewport};
        glUseProgram(*program);
        program_mvp = glm::ortho<float>((int)box.l, (int)box.r, (int)box.u, (int)box.d, 0, 1);
        program_image = 0;
        if (context.princess.health > 0)
                tiles.render();
        sprites.render();

        return color_buffer;
}

// Renders the distortion layer

Distortion_renderer::Distortion_renderer(const Render_config& config) :
        program{vertex_shader(xdg::data::find(BASEDIR "/shaders/geometry.vert")),
                fragment_shader(xdg::data::find(BASEDIR "/shaders/geometry.frag"))},
        program_mvp(program, "mvp"),
        program_image(program, "image")
{
        auto w = (int)pixels16(viewport.x);
        auto h = (int)pixels16(viewport.y);

        glBindFramebuffer(GL_FRAMEBUFFER, *fbo);
        glBindTexture(GL_TEXTURE_2D, *color_buffer);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, w, h, 0, GL_RGBA, GL_FLOAT, NULL);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, *color_buffer, 0);

        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
                throw std::runtime_error{"Framebuffer is not complete!"};
}

void Distortion_renderer::update(Context& context)
{
        distortions.update(context.distortions);
}

const Texture& Distortion_renderer::render(Context& context)
{
        glBindFramebuffer(GL_FRAMEBUFFER, *fbo);
        glClearColor(0.5f, 0.5f, 0.5f, 0.0f);
        glClear(GL_COLOR_BUFFER_BIT);
        glDisable(GL_CULL_FACE);
        glDisable(GL_DEPTH_TEST);

        glViewport(0, 0, (int)pixels16(viewport.x), (int)pixels16(viewport.y));
        pixels16b box{context.offset, context.offset + viewport};
        glUseProgram(*program);
        program_mvp = glm::ortho<float>((int)box.l, (int)box.r, (int)box.u, (int)box.d, 0, 1);
        program_image = 0;

        distortions.render();
        return color_buffer;
}

// Scales the rendered scene to fit the window and---optionally, depending on
// the shaders provided by the configuration---applies a number of effects

Post_processor::Post_processor(const Render_config& config) :
        program{vertex_shader(xdg::data::find(BASEDIR "/shaders/final.vert")),
                fragment_shader(xdg::data::find(BASEDIR "/shaders/final.frag"))},
        program_pixel_size(program, "pixel_size"),
        program_scanlines(program, "scanlines")
{
        scanlines = config.scanlines;
}

void Post_processor::render(const Texture& color, const Texture& distortion)
{
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        glClear(GL_COLOR_BUFFER_BIT);
        glDisable(GL_CULL_FACE);
        glDisable(GL_DEPTH_TEST);
        glUseProgram(*program);

        auto [width, height] = get_viewport();
        program_pixel_size = (float)width / (int)pixels16(viewport.x);
        glViewport(0, 0, width, height);
        program_scanlines = scanlines;

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, *color);
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, *distortion);
        quad.render();
}

// Manages high-level rendering logic, delegating all the work in other
// classes: Geometry_renderer renders the scene at one-pixel-per-pixel size at
// a small texture, and Post_processor scales it up to fit into the screen,
// optionally applying a number of effects.
//
// The update() function is called to rebuild geometry. The sprite geometry
// will always be rebuilt; the tile geometry will only be rebuild if
// context.update_nametable is true.
//
// The render() function renders the geometry that is built at that moment

Renderer::Renderer(const Render_config& config) :
        geometry_renderer(config),
        distortion_renderer(config),
        post_processor(config)
{}

void Renderer::update(Context& context)
{
        geometry_renderer.update(context);
        distortion_renderer.update(context);
}

void Renderer::render(Context& context)
{
        const auto& color = geometry_renderer.render(context);
        const auto& distortion = distortion_renderer.render(context);
        post_processor.render(color, distortion);
}

