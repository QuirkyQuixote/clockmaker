// render.h - declarations for scene rendering

// Copyright (C) 2022 L. Sanz <luis.sanz@gmail.com>

// This file is part of Clockmaker.

// Clockmaker is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Clockmaker is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Clockmaker.  If not, see <https://www.gnu.org/licenses/>.


#ifndef SRC_RENDER_H_
#define SRC_RENDER_H_

#include <vector>

#include "glm/glm.hpp"
#include "geom/packed_table.h"
#include "xdgbds.h"

#include "space.h"
#include "media.h"
#include "config.h"

// Forward declaration, because the actual context requires this file.
struct Context;

// Simple vertex for 2D drawing
struct Vertex {
        GLfloat x, y;   // position
        GLfloat u, v;   // texture coordinates
        GLfloat s;      // special
};

// Holds geometry to render a quad that covers the entire screen
// Can be render by calling render()

class Quad {
 private:
        Vertex_array vao;
        Buffer vbo;

 public:
        Quad();
        void render();
};

// Holds an OpenGL vertex array and vertex buffer.
// The array contents can be modified by calling update().
// The array is rendered by calling render().

class Geometry {
 private:
        Vertex_array vao_;
        Buffer vbo_;
        size_t size_{0};

 public:
        Geometry();
        void update(const Vertex* buf, size_t len);
        void render();
};

// The nametable contains the tiles for the background layer.
using Nametable = geom::Packed_table<cells16v, uint16_t>;

// Manages the background layer, rendered from an array of indices that refer
// to 8x8 elements in the texture of the vertex array.

// The nametable is an array of indices of texture elements, plus some flags.
// It is interpreted as a two-dimensional table of width stride.

// Call update() after modifying the tile table or its stride.
// Call render() to render the current contents of the vertex array.

struct Tile_layer {
        Geometry geometry;
        Texture texture = load_image(xdg::data::find(BASEDIR "/textures/tiles.gif"));

        void update(const Nametable& nametable);
        void render();
};

// Sprites are free-moving elements that are drawn to the screen
struct Sprite {
        pixels16v p;    // position
        uint16_t c;      // index of the texture element plus flags
};

// Bits beyond the eighth in a sprite index are flags.
enum Sprite_flags : uint16_t {
        fliph = 0x8000,         // Flip texture horizontally
        flipv = 0x4000,         // Flip texture vertically
        flipd = 0x2000,         // Flip texture diagonally
        focus = 0x1000,         // Paint white
};

// Manages the sprite layer, rendered from freely-moving 8x8 elements from the
// texture in the vertex array.

// Call update() after modifying the sprite list.
// Call render() to render the current contents of the vertex array.

struct Sprite_layer {
        Geometry geometry;
        Texture texture = load_image(xdg::data::find(BASEDIR "/textures/sprites.gif"));

        void update(const std::vector<Sprite>& sprites);
        void render();
};

// Build data in a sprite list on the fly.
//
// A sprite list consists on a range of sprites, plus a meta-sprite. Each of
// the sprites are transformed in one sprite in the final sprite list, and the
// metasprite determines how the transformation happens.
//
// If the metasprite is flipped, it not only toggles the flip bits of the
// individual sprites, but flips their position in relation to zero.
//
// The updated position for each sprite is then added to the position of the
// metasprite to generate the value that is pushed to the list.

template<std::input_iterator I, std::sentinel_for<I> S>
void push_sprites(std::vector<Sprite>& sprites, I first, S last, Sprite meta)
{
        while (first != last) {
                auto sprite = *first++;
                sprite.c ^= meta.c;
                if (meta.c & Sprite_flags::fliph)
                        sprite.p.x = -(sprite.p.x + 8_px);
                if (meta.c & Sprite_flags::flipv)
                        sprite.p.y = -(sprite.p.y + 8_px);
                sprite.p += meta.p;
                sprites.push_back(sprite);
        }
}

// Push vertices for the given sprite
//
// Each sprite will push six vertices (two triangles) to the vertex list. Their
// positions depend entirely on the sprite position. All sprites are the same
// size (8x8)
//
// The texture coordinates are determined by the texture element
// data and they may be flipped with the appropriate Sprite_flags values.

void push_vertices(std::vector<Vertex>& vertices, Sprite sprite);

// Distortions are free-moving elements that cause the screen to warp
struct Distortion {
        pixels16b box;  // Box where the distortion is drawn
};

// Manages the distortion layer, rendered from freely-moving elements of
// arbitrary size, all with the same texture.

// Call update() after modifying the distortion list.
// Call render() to render the current contents of the vertex array.

struct Distortion_layer {
        Geometry geometry;
        Texture texture = load_image(xdg::data::find(BASEDIR "/textures/distortion.png"));

        void update(const std::vector<Distortion>& distortions);
        void render();
};

// Push distortions to list

template<std::input_iterator I, std::sentinel_for<I> S>
void push_distortions(std::vector<Distortion>& distortions, I first, S last, Distortion meta)
{
        while (first != last) {
                auto dist = *first++;
                dist.box += geom::top_left(meta.box);
                distortions.push_back(dist);
        }
}

// Push vertices for the given distortion

void push_vertices(std::vector<Vertex>& buf, Distortion d);

// Renders geometry

class Geometry_renderer {
 private:
        Frame_buffer fbo;
        Texture color_buffer;
        Program program;
        Uniform<glm::mat4> program_mvp;
        Uniform<GLint> program_image;
        Tile_layer tiles;
        Sprite_layer sprites;

 public:
        Geometry_renderer(const Render_config& config);
        void update(Context& context);
        const Texture& render(Context& context);
};

// Render distortions

class Distortion_renderer {
 private:
        Frame_buffer fbo;
        Texture color_buffer;
        Program program;
        Uniform<glm::mat4> program_mvp;
        Uniform<GLint> program_image;
        Distortion_layer distortions;

 public:
        Distortion_renderer(const Render_config& config);
        void update(Context& context);
        const Texture& render(Context& context);
};

// Post-process the rendered scene

class Post_processor {
 private:
        Quad quad;
        Program program;
        Uniform<GLfloat> program_pixel_size;
        Uniform<GLint> program_scanlines;
        int scanlines;

 public:
        Post_processor(const Render_config& config);
        void render(const Texture& color, const Texture& distortion);
};

// Renders the scene

class Renderer {
 private:
        Geometry_renderer geometry_renderer;
        Distortion_renderer distortion_renderer;
        Post_processor post_processor;

 public:
        Renderer(const Render_config& config);
        void update(Context& context);
        void render(Context& context);
};

#endif          // SRC_RENDER_H_
