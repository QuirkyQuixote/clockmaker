// mixer.h - custom mixer for SDL_audio

// Copyright (C) 2022 L. Sanz <luis.sanz@gmail.com>

// This file is part of Clockmaker.

// Clockmaker is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Clockmaker is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Clockmaker.  If not, see <https://www.gnu.org/licenses/>.

#ifndef SRC_MIXER_H_
#define SRC_MIXER_H_

#include <filesystem>
#include <vector>
#include <chrono>

#include "SDL2/SDL.h"

// Wrapper for data that defines the where and how audio data will be mixed.

struct Stream {
        Uint8* buf;
        int len;
        SDL_AudioFormat format;
};

// A segment of a larger audio sample to be played

struct Segment {
        std::chrono::milliseconds first;
        std::chrono::milliseconds last;
};

// Each audio sample doubles as an audio channel, so the same sample can't be
// mixed twice in the output stream.
//
// In addition to the audio data, each sample contains a queue of segments to
// play. If the sample is not playing, the queue is empty; if the sample is
// supposed to be played once, the queue contains one segment covering the
// entire audio data, etc.
//
// There are a number of functions that can manipulate the queue:
//
// - discard_all() discards all segments, including the one currently playing.
// - discard_queue() discard all segments but the one currently playing.
// - push() pushes a segment to the queue.
// - size() returns the number of queued segments.

class Sample {
 private:
        struct Segment {
                size_t first;
                size_t last;
        };

        std::vector<Uint8> data;
        std::deque<Segment> segments;

        bool mix_(const Stream& stream, Segment& s)
        {
                int len = std::min<int>(stream.len, s.last - s.first);
                SDL_MixAudioFormat(stream.buf, data.data() + s.first, stream.format,
                                len, SDL_MIX_MAXVOLUME);
                s.first += len;
                return s.first == s.last;
        }

 public:
        Sample() = default;
        Sample(const std::vector<Uint8>& new_data) { reset(new_data); }

        void reset(const std::vector<Uint8>& new_data)
        {
                data = new_data;
                segments.clear();
        }

        size_t size() { return segments.size(); }

        void discard_all() { segments.clear(); }
        void discard_queued() { if (segments.size() > 1) segments.resize(1); }
        void push() { segments.emplace_back(0, data.size()); }
        void push(size_t first, size_t last) { segments.emplace_back(first, last); }

        void mix(const Stream& stream)
        {
                while (!segments.empty() && mix_(stream, segments.front()))
                        segments.pop_front();
        }
};

// Load a sample from a file.
//
// This function will copy the loaded audio data into an std::vector and then
// destroy the array returned by SDL_LoadWAV(). It makes life much easier in
// the long term.

inline std::pair<std::vector<Uint8>, SDL_AudioSpec> load_sample(
                const std::filesystem::path& path)
{
        SDL_AudioSpec spec;
        Uint8* buf;
        Uint32 len;
        if (SDL_LoadWAV(path.string().c_str(), &spec, &buf, &len) == NULL)
                throw std::runtime_error{SDL_GetError()};
        std::vector<Uint8> data(buf, buf + len);
        SDL_FreeWAV(buf);
        return std::make_pair(data, spec);
}

// Convert a sample between two audio specifications
//
// Will raise an exception if the conversion is not possible. If no conversion
// is required, returns the unmodified data. If conversion is possible,
// transforms data and returns a reference to it, so if you want the original
// data for something you'll need to pass a copy.

inline std::vector<Uint8>& convert_sample(std::vector<Uint8>& data,
                const SDL_AudioSpec& from, const SDL_AudioSpec& to)
{
        SDL_AudioCVT cvt;
        if (SDL_BuildAudioCVT(&cvt, from.format, from.channels, from.freq,
                                to.format, to.channels, to.freq) < 0)
                throw std::runtime_error{SDL_GetError()};
        if (cvt.needed) {
                cvt.len = data.size();
                data.resize(cvt.len * cvt.len_mult);
                cvt.buf = data.data();
                SDL_ConvertAudio(&cvt);
                data.resize(cvt.len_cvt);
        }
        return data;
}

// The actual mixer keeps:
//
// - an audio device to mix stuff into,
// - a pool of samples that can be mixed into the stream,
//
// And is registered on the SDL device to be called back every time more data
// needs to be mixed; when this happens, the mixer will iterate on ever open
// stream, and request it to mix their data.

class Mixer {
 private:
        SDL_AudioSpec spec;
        SDL_AudioDeviceID device = 0;
        std::vector<Sample> samples;

        void mix(Uint8* buf, int len)
        {
                std::fill_n(buf, len, 0);
                Stream stream{buf, len, spec.format};
                for (auto& sample : samples) sample.mix(stream);
        }

        static void mix_samples(void* udata, Uint8* buf, int len)
        { reinterpret_cast<Mixer*>(udata)->mix(buf, len); }

 public:
        Mixer() = default;
        Mixer(const SDL_AudioSpec& want) { open(want); }

        ~Mixer() { close(); }

        void open(const SDL_AudioSpec& want)
        {
                SDL_AudioSpec temp = want;
                temp.callback = mix_samples;
                temp.userdata = this;
                device = SDL_OpenAudioDevice(NULL, 0, &temp, &spec,
                                SDL_AUDIO_ALLOW_ANY_CHANGE);
                if (device == 0) throw std::runtime_error{SDL_GetError()};
                SDL_PauseAudioDevice(device, 0);
        }

        void close()
        {
                if (device == 0) return;
                SDL_PauseAudioDevice(device, 1);
                SDL_CloseAudioDevice(device);
                device = 0;
        }

        size_t load(const std::filesystem::path& path)
        {
                size_t n = samples.size();
                auto [file_data, file_spec] = load_sample(path);
                samples.push_back(convert_sample(file_data, file_spec, spec));
                return n;
        }

        size_t size(size_t n) { return samples[n].size(); }

        void discard_all(size_t n) { samples[n].discard_all(); }
        void discard_queued(size_t n) { samples[n].discard_queued(); }
        void push(size_t n) { samples[n].push(); }

        void push(size_t n, const Segment& p)
        {
                auto bytes_per_sample = spec.size / spec.samples;
                auto bytes_per_second = bytes_per_sample * spec.freq;
                samples[n].push((p.first.count() * bytes_per_second) / 1000,
                                (p.last.count() * bytes_per_second) / 1000);
        }

        template<std::input_iterator I, std::sentinel_for<I> S>
        void push(size_t n, I first, S last)
        { while (first != last) push(n, *first++); }

        template<std::ranges::input_range R>
        void push(size_t n, R r)
        { push(n, std::ranges::begin(r), std::ranges::end(r)); }

        template<class... Args>
        void play(size_t n, Args&&... args)
        {
                discard_all(n);
                push(n, std::forward<Args>(args)...);
        }

        template<class... Args>
        void enqueue(size_t n, Args&&... args)
        {
                discard_queued(n);
                push(n, std::forward<Args>(args)...);
        }
};

#endif // SRC_MIXER_H_
