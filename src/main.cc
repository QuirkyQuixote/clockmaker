// main.cc - application entrypoint

// Copyright (C) 2022 L. Sanz <luis.sanz@gmail.com>

// This file is part of Clockmaker.

// Clockmaker is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Clockmaker is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Clockmaker.  If not, see <https://www.gnu.org/licenses/>.


#include <iostream>
#include <getopt.h>

#include "config.h"
#include "context.h"
#include "game.h"
#include "xdgbds.h"
#include "input.h"

const char usage_string[] =
"Usage: clockmaker --help\n"
"       clockmaker --version\n"
"       clockmaker --config\n"
"       clockmaker --input\n"
"       clockmaker\n"
"\n";

const char option_string[] =
"Options:\n"
"  -h, --help           display this help and exit\n"
"  -v, --version        display version string and exit\n"
"  -c, --config         create configuration file\n"
"  -i, --input          query inputs and create configuration file\n"
"\n";

Config load_default_config()
{
        Config config;
        try {
                config = load_config(xdg::config::find(BASEDIR ".json"));
                config.window.fps = 30;
        } catch (std::exception& ex) {
                std::cerr << ex.what() << "\n";
                std::cerr << "Defaulting to hardcoded configuration.\n";
        }
        return config;
}

void save_default_config(const Config& config)
{
        auto path = xdg::config::home() / BASEDIR ".json";
        save_config(config, path);
        std::cout << "Config file saved to " << path.string() << "\n\n";
}

void query_and_save_config()
{
        Config config = load_default_config();
        Query_inputs query_inputs;
        config.input.left = query_inputs("left");
        config.input.right = query_inputs("right");
        config.input.up = query_inputs("up");
        config.input.down = query_inputs("down");
        config.input.jump = query_inputs("jump");
        config.input.attack = query_inputs("attack");
        save_default_config(config);
}

bool parse_options(int argc, char* argv[])
{
        static struct option options[] = {
                { "help",       no_argument, 0, 'h' },
                { "version",    no_argument, 0, 'v' },
                { "config",     no_argument, 0, 'c' },
                { "input",      no_argument, 0, 'i' },
                { 0,            0,           0,  0  },
        };

        switch (getopt_long(argc, argv, "hvci", options, NULL)) {
         case -1:
                return false;
         case 'h':
                std::cout << usage_string;
                std::cout << option_string;
                exit(EXIT_SUCCESS);
         case 'v':
                std::cout << VERSION << "\n";
                exit(EXIT_SUCCESS);
         case 'c':
                save_default_config(Config{});
                exit(EXIT_SUCCESS);
         case 'i':
                query_and_save_config();
                exit(EXIT_SUCCESS);
         default:
                return true;
        }
}

int main(int argc, char *argv[])
{
        while (parse_options(argc, argv))
                continue;

        try {
                Context context = load_context(xdg::data::find(BASEDIR "/levels/untitled.json"));
                Update{load_default_config(), context}();
                return 0;
        } catch (std::exception& ex) {
                std::cerr << ex.what() << "\n";
                return -1;
        }
        return 0;
}
