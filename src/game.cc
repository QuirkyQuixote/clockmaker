// game.cc - main game logic

// Copyright (C) 2022 L. Sanz <luis.sanz@gmail.com>

// This file is part of Clockmaker.

// Clockmaker is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Clockmaker is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Clockmaker.  If not, see <https://www.gnu.org/licenses/>.


#include "game.h"

#include "xdgbds.h"
#include "export.h"

Audio::Audio(const Audio_config& conf)
{
        SDL_AudioSpec spec = {
                conf.frequency,
                conf.format,
                (Uint8)conf.channels,
                0,
                (Uint16)conf.chunksize,
        };

        mixer.open(spec);

        sounds.push_back(mixer.load(xdg::data::find(BASEDIR "/sounds/bounce.wav")));
        sounds.push_back(mixer.load(xdg::data::find(BASEDIR "/sounds/hurt.wav")));
        sounds.push_back(mixer.load(xdg::data::find(BASEDIR "/sounds/jump.wav")));
        sounds.push_back(mixer.load(xdg::data::find(BASEDIR "/sounds/land.wav")));
        sounds.push_back(mixer.load(xdg::data::find(BASEDIR "/sounds/step.wav")));
        sounds.push_back(mixer.load(xdg::data::find(BASEDIR "/sounds/swing.wav")));
        sounds.push_back(mixer.load(xdg::data::find(BASEDIR "/sounds/mob_die.wav")));
        sounds.push_back(mixer.load(xdg::data::find(BASEDIR "/sounds/coin.wav")));
        sounds.push_back(mixer.load(xdg::data::find(BASEDIR "/sounds/death.wav")));
        sounds.push_back(mixer.load(xdg::data::find(BASEDIR "/sounds/ping.wav")));

        music = mixer.load(xdg::data::find(BASEDIR "/music/zwosta.wav"));
}

Audio::~Audio() { }

void Audio::update(Context& context)
{
        for (size_t i = 0; i < sounds.size(); ++i)
                if (context.sounds[i])
                        mixer.play(sounds[i]);
        context.sounds = 0;

        if (!context.music.empty()) {
                mixer.enqueue(music, context.music);
                context.music.clear();
        }

        context.empty_music_queue = mixer.size(music) <= 1;
}

// General camera updating is very simple:
// 
// if it's outside the current room, update with outside(), then return enter
// if the camera reached the room and outside if not.
//
// if it's inside the current room, update with inside(), then return exit if
// the camera exited the room and inside if not.

Camera::Camera(Context& context)
{
        inside(context);
        context.offset = offset;
}

Camera::State Camera::update(Context& context)
{
        if (scrolling) {
                outside(context);
                return scrolling ? State::outside : State::enter;
        }
        inside(context);
        return scrolling ? State::exit : State::inside;
}

void Camera::outside(Context& context)
{
        if (geom::abs(offset.x - context.offset.x) > 8_px)
                context.offset.x = (context.offset.x + offset.x) / 2;
        else
                context.offset.x = offset.x;
        if (geom::abs(offset.y - context.offset.y) > 8_px)
                context.offset.y = (context.offset.y + offset.y) / 2;
        else
                context.offset.y = offset.y;
        if (offset == context.offset)
                scrolling = false;
}

void Camera::inside(Context& context)
{
        if (geom::intersects(context.princess.body.box, context.room.box)) {
                context.offset = recalculate_offset(context);
                return;
        }
        for (auto& r : context.rooms) {
                if (geom::intersects(context.princess.body.box, r.box)) {
                        context.room = r;
                        break;
                }
        }
        offset = recalculate_offset(context);
        scrolling = true;
}

pixels16v Camera::recalculate_offset(Context& context)
{
        pixels16v ret = top_left(context.princess.body.box) - (viewport / 2);
        pixels16b box(context.room.box.min(), context.room.box.max() - viewport);
        ret.x = std::clamp(ret.x, box.l, box.r);
        ret.y = std::clamp(ret.y, box.d, box.u);
        return ret;
}

// Manages events for the game
//
// At creation time, the class opens every single joystick and game controller
// it can find. This is probably not a good idea, but I'm still experimenting
// with the concept a little.
//
// The update function will consume every queued event from SDL and pass them
// to the event catchers defined in the configuration table, then update the
// input struct with the resulting values.

Event_handler::Event_handler(const Input_config& config) : config{config}
{
        gamepads = open_all_gamepads();
}

void Event_handler::update(Input& input)
{
        SDL_Event event;
        while (SDL_PollEvent(&event))
                handle_event(event, input);
}

void Event_handler::handle_event(const SDL_Event& event, Input& input)
{
        static const std::vector<Event_catcher> quit = {
                Scancode_catcher{SDL_SCANCODE_ESCAPE},
                Quit_catcher{}
        };

        input.left = catch_event(event, config.left, input.left);
        input.right = catch_event(event, config.right, input.right);
        input.up = catch_event(event, config.up, input.up);
        input.down = catch_event(event, config.down, input.down);
        input.jump = catch_event(event, config.jump, input.jump);
        input.attack = catch_event(event, config.attack, input.attack);
        input.start = catch_event(event, config.start, input.start);
        input.terminate = catch_event(event, quit, input.terminate);
}

// The update process starts by moving the camera; the rest depends on what the
// camera returned:
//
// When the camera enters a new room, the princess position is stored in the
// room_spawn variable, and the mob list is populated from the mob places that
// are inside the room.  
//
// If the camera remains inside a room, the game runs: the mobs do things, the
// princess does as the input says... unless the game is frozen, of course.
//
// If the camera exits the current room, all mobs are removed and the sprite
// list is populated only by the princess sprites; the list will remain so
// until the next room is entered.
//
// If the camera is outside the current room, do nothing. The sprites for the
// princess are already in the sprite list, so let this cycle pass and wait
// until the next room is entered.

void update_message(Context& context)
{
        if (context.message.fade > std::max<int>(context.message.text.size(), 60))
                return;
        if (context.message.text.empty())
                return;
        if (context.message.cur < static_cast<int>(context.message.text.size()))
                context.message.cur += 2;
        else
                ++context.message.fade;

        geom::Packed_table<cells16v, char> text(cells16v(), cells16v(viewport.x, 5_cl), ' ');
        cells16v offset(4, 1);
        for (auto c : context.message.text | std::views::take(context.message.cur)) {
                if (c == '\n') offset = cells16v(4_cl, offset.y + 1_cl);
                else { text[offset] = c; ++offset.x; }
        }

        Sprite meta = {context.offset, 0};
        offset = cells16v{0_cl, viewport.y - 5_cl};
        std::vector<Sprite> sprites;
        for (auto c : text) {
                sprites.emplace_back(offset, 1024 - 128 + c);
                if (++offset.x == viewport.x) {
                        offset.x = 0_cl;
                        ++offset.y;
                }
        }
        push_sprites(context.sprites, sprites.begin(), sprites.end(), meta);
}

Update::Update(const Config& config, Context& context) :
        Main_loop{config.window},
        context{context},
        audio{config.audio},
        camera{context},
        renderer{config.render},
        event_handler{config.input}
{
        context.lua_state = luaL_newstate();
        luaL_openlibs(context.lua_state);
        export_lua_bindings(context);
        auto path = xdg::data::find(BASEDIR "/levels/untitled.lua");
        ishtar::run(context.lua_state, path);
        level_spawn = context.princess.body.box;
}

bool Update::update()
{
        event_handler.update(context.input);
        switch (camera.update(context)) {
         case Camera::State::enter: enter_room(); break;
         case Camera::State::inside: inside_room(); break;
         case Camera::State::exit: exit_room(); break;
         case Camera::State::outside: outside_room(); break;
        }
        renderer.render(context);
        audio.update(context);
        ++context.frame;
        return context.input.terminate;
}

void Update::enter_room()
{
        room_spawn = context.princess.body.box;
        load_mobs(context);
}

void Update::inside_room()
{
        if (context.freeze) {
                --context.freeze;
                return;
        }
        if (context.princess.health <= 0) {
                context.princess.body.vel = {0, 0};
                context.princess.health = 127;
                context.princess.body.box = level_spawn;
                return;
        }
        context.sprites.clear();
        context.distortions.clear();
        update_mobs(context);
        update_princess(context);
        update_message(context);
        renderer.update(context);
}

void Update::exit_room()
{
        context.sprites.clear();
        context.message.text.clear();
        clear_mobs(context);
        update_princess(context);
        update_message(context);
        renderer.update(context);
}

void Update::outside_room()
{
}

