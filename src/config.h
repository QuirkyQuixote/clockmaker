// config.h - game configuration

// Copyright (C) 2022 L. Sanz <luis.sanz@gmail.com>

// This file is part of Clockmaker.

// Clockmaker is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Clockmaker is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Clockmaker.  If not, see <https://www.gnu.org/licenses/>.


#ifndef SRC_CONFIG_H_
#define SRC_CONFIG_H_

#include <filesystem>

#include "event.h"
#include "media.h"

// Configuration 

struct Audio_config {
        int frequency = 22050;
        Uint16 format = AUDIO_S16;
        int channels = 2;
        int chunksize = 1024;
        int music_volume = 127;
        int sfx_volume = 127;
};

struct Render_config {
        bool blur = false;
        int scanlines = 1;
};

struct Input_config {
        std::vector<Event_catcher> left{
                Controller_button_catcher{0, SDL_CONTROLLER_BUTTON_DPAD_LEFT},
                Scancode_catcher{SDL_SCANCODE_LEFT}};
        std::vector<Event_catcher> right{
                Controller_button_catcher{0, SDL_CONTROLLER_BUTTON_DPAD_RIGHT},
                Scancode_catcher{SDL_SCANCODE_RIGHT}};
        std::vector<Event_catcher> up{
                Controller_button_catcher{0, SDL_CONTROLLER_BUTTON_DPAD_UP},
                Scancode_catcher{SDL_SCANCODE_UP}};
        std::vector<Event_catcher> down{
                Controller_button_catcher{0, SDL_CONTROLLER_BUTTON_DPAD_DOWN},
                Scancode_catcher{SDL_SCANCODE_DOWN}};
        std::vector<Event_catcher> jump{
                Controller_button_catcher{0, SDL_CONTROLLER_BUTTON_A},
                Scancode_catcher{SDL_SCANCODE_X}};
        std::vector<Event_catcher> attack{
                Controller_button_catcher{0, SDL_CONTROLLER_BUTTON_X},
                Scancode_catcher{SDL_SCANCODE_Z}};
        std::vector<Event_catcher> start{
                Controller_button_catcher{0, SDL_CONTROLLER_BUTTON_START},
                Scancode_catcher{SDL_SCANCODE_RETURN}};
};

struct Config {
        Window_config window;
        Audio_config audio;
        Render_config render;
        Input_config input;
};

Config load_config(const std::filesystem::path& path);

void save_config(const Config& config, const std::filesystem::path& path);

#endif // SRC_CONFIG_H_
