// context.h - declare game context

// Copyright (C) 2022 L. Sanz <luis.sanz@gmail.com>

// This file is part of Clockmaker.

// Clockmaker is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Clockmaker is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Clockmaker.  If not, see <https://www.gnu.org/licenses/>.


#ifndef SRC_CONTEXT_H_
#define SRC_CONTEXT_H_

#include <vector>
#include <filesystem>
#include <bitset>
#include <deque>
#include <chrono>
#include <list>

#include "ishtar.h"

#include "physics.h"
#include "render.h"
#include "mobs.h"
#include "princess.h"
#include "mixer.h"

// size of visible area, in tiles
static const cells16v viewport{48, 27};

// Defines a room
struct Room {
        points32b box{0, 0, 0, 0};
        size_t first_place{0};
        size_t last_place{0};
};

// Input mask
struct Input {
        bool up = false;
        bool down = false;
        bool left = false;
        bool right = false;
        bool jump = false;
        bool attack = false;
        bool start = false;
        bool terminate = false;
};

// Indices of all sound effects
enum Sound {
        bounce, hurt, jump, land, step, swing, mob_die, coin, death, ping
};

// Text output
struct Message {
        std::string text;               // Text being displayed on screen
        int cur;                        // Last character of message being printed
        int fade;                       // Time until message fades
};

// Extra data required for the mobs
struct Context {
        Input input;                    // Defines which inputs are toggled.
        Princess princess;              // Data for the player character
        std::vector<Room> rooms;        // List of all rooms in the scene
        Room room;                      // Current room in the scene
        pixels16v offset{0, 0};         // Top left corner of visible area
        std::vector<Mob_place> places;  // Places where mobs spawn on room change
        std::list<Mob> mobs;            // Mobs actually loaded
        Hitmask hitmask;                // hitmask table
        Nametable nametable;            // background layer
        bool update_nametable{true};    // if true, the nametable has been changed
        std::vector<Sprite> sprites;    // sprite layer
        std::vector<Distortion> distortions;// distortion layer
        int frame = 0;                  // Current frame
        int freeze = 0;                 // If greater than zero, the game pauses
        std::bitset<32> sounds;         // Mask of sounds to be played.
        Message message;                // Text display
        lua_State* lua_state;           // Lua state
        std::deque<Segment> music;      // Queue of music segments to play
        bool empty_music_queue{true};   // If true, the music queue is running dry
};

Context load_context(const std::filesystem::path& path);

#endif // SRC_CONTEXT_H_
