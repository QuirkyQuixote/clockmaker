// mobs.h - types to describe movable objects

// Copyright (C) 2022 L. Sanz <luis.sanz@gmail.com>

// This file is part of Clockmaker.

// Clockmaker is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Clockmaker is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Clockmaker.  If not, see <https://www.gnu.org/licenses/>.


#ifndef SRC_MOBS_H_
#define SRC_MOBS_H_

#include <bitset>

#include "ishtar.h"

#include "physics.h"

// Forward declaration
struct Context;

// Defines a place where a mob will appear
struct Mob_place {
        int id;
        points32b box;
        std::string name;
};

// Flags common to all mobs
enum Mob_flags : uint32_t {
        mf_create,
        mf_focus,
        mf_trigger
};

// Any mob
struct Mob {
 private:
        lua_State* lua_state = NULL;    // Lua state
        int lua_ref = -1;               // reference to Lua table
        std::bitset<4> event;           // event flags

        template<class T> std::optional<T> get_(const char* name) const;
        bool call_(const char* event);

 public:
        Mob() = default;
        Mob(const Mob&) = delete;
        Mob(Mob&& r)
        {
                std::swap(lua_state, r.lua_state);
                std::swap(lua_ref, r.lua_ref);
                std::swap(event, r.event);
        }

        Mob(lua_State* lua_state, const std::string& name, int place, const points32b& box);

        ~Mob()
        {
                if (lua_state) {
                        call_("destroy");
                        luaL_unref(lua_state, LUA_REGISTRYINDEX, lua_ref);
                }
        }

        Mob& operator=(const Mob&) = delete;
        Mob& operator=(Mob&& r)
        {
                std::swap(lua_state, r.lua_state);
                std::swap(lua_ref, r.lua_ref);
                std::swap(event, r.event);
                return *this;
        }

        bool update(Context& context);
};

Mob& make_mob(Context& context, const std::string& name, const points32b& box);

void clear_mobs(Context& context);
void update_mobs(Context& context);
void load_mobs(Context& context);

#endif // SRC_MOBS_H_
