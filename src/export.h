// export.h - Lua bindings

// Copyright (C) 2022 L. Sanz <luis.sanz@gmail.com>

// This file is part of Clockmaker.

// Clockmaker is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Clockmaker is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Clockmaker.  If not, see <https://www.gnu.org/licenses/>.

#ifndef SRC_EXPORT_H_
#define SRC_EXPORT_H_

#include <algorithm>

#include "geom/math.h"

std::string justify(std::string message)
{
        if (message.find('\n') != std::string::npos)
                return message;
        std::string::size_type a = 0, b = 0;
        for (int i = 0; i < 3; ++i) {
                if (message.size() < a + 40)
                        return message;
                b = message.rfind(' ', a + 40);
                if (b == std::string::npos)
                        return message;
                message[b] = '\n';
                a = b;
        }
        return message;
}

struct Export_context {
        static void say_(Context* context, const std::string& message)
        {
                context->message.text = justify(message);
                context->message.cur = 0;
                context->message.fade = 0;
        }

        static void sprites_(Context* context, const points32b& box, uint16_t flags,
                        const std::vector<std::array<int16_t, 3>>& data)
        {
                auto first = reinterpret_cast<const Sprite*>(data.data());
                Sprite meta{geom::center(box), flags};
                push_sprites(context->sprites, first, first + data.size(), meta);
        }

        static void distortions_(Context* context, const points32b& box, uint16_t flags,
                        const std::vector<std::array<int16_t, 4>>& data)
        {
                auto first = reinterpret_cast<const Distortion*>(data.data());
                Distortion meta{box};
                push_distortions(context->distortions, first, first + data.size(), meta);
        }

        static void sound_(Context* context, int sound)
        { context->sounds[sound] = true; }

        static void music_(Context* context, int first, int size, int count)
        {
                context->music.clear();
                while (count--) {
                        context->music.emplace_back(
                                        std::chrono::milliseconds{first},
                                        std::chrono::milliseconds{first + size});
                        first += size;
                }
        }

        static bool needs_music_(Context* context)
        { return context->empty_music_queue; }

        static points32b pc_box_(Context* context) { return context->princess.body.box; }
        static points32v pc_vel_(Context* context) { return context->princess.body.vel; }
        static bool pc_harm_(Context* context, const points32b& box)
        { return harm_princess(*context, box); }
        static points32b pc_target_(Context* context) { return context->princess.target_box; }
        static points32b pc_attack_(Context* context) { return context->princess.attack_box; }

        static int hitmask_(lua_State* state)
        {
                try {
                        auto context = ishtar::to<Context*>(state, 1);
                        auto box = ishtar::to<cells16b>(state, 2);
                        auto slice = geom::slice(context->hitmask, box);
                        bool val = lua_tointeger(state, 3);
                        for (auto x : slice) x = val;
                } catch (std::exception& ex) {
                        std::cerr << ex.what() << "\n";
                }
                return 0;
        }

        static std::pair<points32b, boolb> move_mob_(Context* context,
                        const points32b& box, const points32v& vel)
        {
                Body body{box, vel};
                move_body(body, context->hitmask);
                return std::make_pair(body.box, body.contact);
        }

        static pixels16b screen_(Context* context)
        { return pixels16b(context->offset, context->offset + viewport); }

        static Mob* make_mob_(Context* context, std::string name, const points32b& box)
        { return &::make_mob(*context, name, box); }

        void operator()(lua_State* l) const
        {
                ishtar::globals(l)
                        .set("Context", ishtar::type<Context*>(l)
                                .set("say", say_)
                                .set("sprites", sprites_)
                                .set("distortions", distortions_)
                                .set("sound", sound_)
                                .set("music", music_)
                                .set("needs_music", needs_music_)
                                .set("hitmask", hitmask_)
                                .set("pc_box", pc_box_)
                                .set("pc_vel", pc_vel_)
                                .set("pc_harm", pc_harm_)
                                .set("pc_target", pc_target_)
                                .set("pc_attack", pc_attack_)
                                .set("move_mob", move_mob_)
                                .set("screen", screen_)
                                .set("make_mob", make_mob_)
                            )
                        ;
        }
};

static constexpr const Export_context export_context;

template<geom::number T> struct Export_num {
        using I = typename T::rep;

        const char* name;

        static T _add(const T& l, const T& r) { return l + r; }
        static T _sub(const T& l, const T& r) { return l - r; }
        static T _mul(const T& l, I r) { return l * r; }
        static T _div(const T& l, I r) { return l / r; }
        static T _mod(const T& l, I r) { return l % r; }
        static bool _eq(const T& l, const T& r) { return l == r; }
        static bool _le(const T& l, const T& r) { return l <= r; }
        static bool _lt(const T& l, const T& r) { return l < r; }
        static I _get(const T& l) { return geom::get(l); }

        static T _cast(std::variant<cells16, pixels16, points32> x)
        { return std::visit([](auto&& arg){ return T{arg}; }, x); }

        void operator()(lua_State* l) const
        {
                ishtar::globals(l)
                        .set(name, ishtar::type<T>(l)
                                .template set_constructor<I>()
                                .set(ishtar::add, _add)
                                .set(ishtar::sub, _sub)
                                .set(ishtar::mul, _mul)
                                .set(ishtar::div, _div)
                                .set(ishtar::mod, _mod)
                                .set(ishtar::eq, _eq)
                                .set(ishtar::le, _le)
                                .set(ishtar::lt, _lt)
                                .set("get", _get)
                            )
                        .set(std::string("to_") + name, _cast)
                        ;
        }
};

static constexpr const Export_num<cells16> export_cells{"cells"};
static constexpr const Export_num<pixels16> export_pixels{"pixels"};
static constexpr const Export_num<points32> export_points{"points"};

template<geom::number T> struct Export_vec {
        using V = geom::Vec<T, 2>;
        using I = typename T::rep;

        const char* name;

        static V _add(const V& l, const V& r) { return l + r; }
        static V _sub(const V& l, const V& r) { return l - r; }
        static V _mul(const V& l, I r) { return l * r; }
        static V _div(const V& l, I r) { return l / r; }
        static V _mod(const V& l, I r) { return l % r; }
        static bool _eq(const V& l, const V& r) { return l == r; }
        static auto _clamp(const V& l, const V& r)
        { return V(std::clamp(l.x, -r.x, r.x), std::clamp(l.y, -r.y, r.y)); }

        static V _cast(std::variant<cells16v, pixels16v, points32v> x)
        { return std::visit([](auto&& arg){ return V{arg}; }, x); }

        void operator()(lua_State* l) const
        {
                ishtar::globals(l)
                        .set(name, ishtar::type<V>(l)
                                .template set_constructor<I, I>()
                                .set(ishtar::add, _add)
                                .set(ishtar::sub, _sub)
                                .set(ishtar::mul, _mul)
                                .set(ishtar::div, _div)
                                .set(ishtar::mod, _mod)
                                .set(ishtar::eq, _eq)
                                .set("x", &V::x)
                                .set("y", &V::y)
                                .set("clamp", _clamp)
                            )
                        .set(std::string("to_") + name, _cast)
                        ;
        }
};

static constexpr const Export_vec<cells16> export_cellsv{"cellsv"};
static constexpr const Export_vec<pixels16> export_pixelsv{"pixelsv"};
static constexpr const Export_vec<points32> export_pointsv{"pointsv"};

template<geom::number T> struct Export_box {
        using B = geom::Box<T, 2>;
        using V = geom::Vec<T, 2>;
        using I = typename T::rep;

        const char* name;

        static B _add(const B& l, const V& r) { return l + r; }
        static B _sub(const B& l, const V& r) { return l - r; }
        static B _mul(const B& l, I r) { return l * r; }
        static B _div(const B& l, I r) { return l / r; }
        static B _mod(const B& l, I r) { return l % r; }
        static bool _eq(const B& l, const B& r) { return l == r; }
        static V _ul(const B& l) { return geom::top_left(l); }
        static V _ur(const B& l) { return geom::top_right(l); }
        static V _dl(const B& l) { return geom::bottom_left(l); }
        static V _dr(const B& l) { return geom::bottom_right(l); }
        static V _up(const B& l) { return geom::top(l); }
        static V _dn(const B& l) { return geom::bottom(l); }
        static V _lf(const B& l) { return geom::left(l); }
        static V _rt(const B& l) { return geom::right(l); }
        static V _center(const B& l) { return geom::center(l); }
        static bool _intersects(const B& l, const B& r)
        { return geom::intersects(l, r); }

        static B _cast(std::variant<cells16b, pixels16b, points32b> x)
        { return std::visit([](auto&& arg){ return B{arg}; }, x); }

        void operator()(lua_State* l) const
        {
                ishtar::globals(l)
                        .set(name, ishtar::type<B>(l)
                                .template set_constructor<I, I, I, I>()
                                .set(ishtar::add, _add)
                                .set(ishtar::sub, _sub)
                                .set(ishtar::mul, _mul)
                                .set(ishtar::div, _div)
                                .set(ishtar::mod, _mod)
                                .set(ishtar::eq, _eq)
                                .set("l", &B::l)
                                .set("r", &B::r)
                                .set("u", &B::u)
                                .set("d", &B::d)
                                .set("ul", _ul)
                                .set("ur", _ur)
                                .set("dl", _dl)
                                .set("dr", _dr)
                                .set("up", _up)
                                .set("dn", _dn)
                                .set("lf", _lf)
                                .set("rt", _rt)
                                .set("center", _center)
                                .set("intersects", _intersects)
                        )
                        .set(std::string("to_") + name, _cast)
                        ;
        }
};

static constexpr const Export_box<cells16> export_cellsb{"cellsb"};
static constexpr const Export_box<pixels16> export_pixelsb{"pixelsb"};
static constexpr const Export_box<points32> export_pointsb{"pointsb"};

struct Export_boolb {
        static bool _eq(const boolb& l, const boolb& r) { return l == r; }

        void operator()(lua_State* l) const
        {
                ishtar::globals(l)
                        .set("boolb", ishtar::type<boolb>(l)
                                .set_constructor<bool, bool, bool, bool>()
                                .set(ishtar::eq, _eq)
                                .set("l", &boolb::l)
                                .set("r", &boolb::r)
                                .set("u", &boolb::u)
                                .set("d", &boolb::d)
                            )
                        ;
        }
};

static constexpr const Export_boolb export_boolb;


void export_lua_bindings(Context& context)
{
        export_cells(context.lua_state);
        export_pixels(context.lua_state);
        export_points(context.lua_state);

        export_cellsv(context.lua_state);
        export_pixelsv(context.lua_state);
        export_pointsv(context.lua_state);

        export_cellsb(context.lua_state);
        export_pixelsb(context.lua_state);
        export_pointsb(context.lua_state);
        export_boolb(context.lua_state);

        export_context(context.lua_state);

        ishtar::globals(context.lua_state)
                .set("context", &context)
                .set("sprite_flipx", (int)Sprite_flags::fliph)
                .set("sprite_flipy", (int)Sprite_flags::flipv)
                .set("sprite_flipz", (int)Sprite_flags::flipd)
                .set("sprite_focus", (int)Sprite_flags::focus)
                ;
}

#endif // SRC_EXPORT_H_
